
## Murine Pneumonia Project


This is the code based used to analyze data associated with the manuscript “Murine 
Host Response to Typical and Atypical Pneumonia” by Matthew S. McCravy and colleagues. 
We analyze gene expression profiles from mice exposed to typical bacteria, atypical
bacteria, and virus using machine learning techniques. We construction a predictive 
signature to differentiate these infectious etiologies. We then apply this predictive 
signature to open-source human gene expression data with excellent predictive accuracy.
 

All raw gene expression files and associated phenotypic data are available from the
Gene Expression Omnibus database (accession number <a href="https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE214051">GSE214051</a>).
User will need to update directory to working folder e.g. ``` setwd("<set/path/to/working/directory/>") ``` 


There are a couple loops that are computationally taxing and time consuming (LOOCV Lasso modeling, external GEO datasets).  As such, output variables have been saved and used, from the 'variables' folder. To rerun these loops, or customize with desired parameters, go to the relevant section and remove ``` "eval=FALSE" ``` from the markdown section header.


The following libraries will be needed:

* library(devtools)
* library(factoextra)
* library(limma)
* library(affy)
* library(readxl)
* library(ggplot2) 
* library(AnnotationDbi)
* library(dplyr)
* library(clusterProfiler)
* library(DOSE)
* library(org.Mm.eg.db)
* library(enrichplot)
* library(glmnet)
* library(ROCR)
* library(e1071)
* library(caret)
* library(OptimalCutpoints)
* library(pROC)
* library(knitr)
* library(kableExtra)
* library(gridExtra)
* library(RColorBrewer)
* library(forcats)
* library(stringr)
* library(mouse430a2.db)
* library(ReactomePA)
* library(biomaRt)
* library(GEOquery)
* library(DT)
* library(ggrepel)
* library(ggpubr)




